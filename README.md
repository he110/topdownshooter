# TopDownShooter

Игра в жанре TDS, разрабатываемая в качестве учебного проекта в рамках программы Skillbox "Профессия Разработчик игр на Unreal Engine 4. Часть 2"

Прогресс реализации можно отслеживать через [Gitlab Boards](https://gitlab.com/he110/topdownshooter/-/boards)